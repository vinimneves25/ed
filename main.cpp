#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

// PILHA
// Implementar os metodos: empilhar, exibir, desempilhar_topo, esvaziar

typedef struct {
    int topo;
    int vetor[10];
}Pilha;

void empilhar(Pilha *pilha);
void listar(Pilha *pilha);
void desempilhar(Pilha *pilha);
void esvaziar(Pilha *pilha);

int main() {
    Pilha pilha[0];
    pilha->topo = 0;
    
    int resp;
    do {
        system("clear");
        printf("\nInforme o que fazer:\n[1] - empilhar   [2] - exibir (5 segundos)   [3] - desempilhar   [4] - esvaziar   [0] - sair\n");
        scanf("%d", &resp);
        
        switch (resp) {
            case 0:
                exit(0);
            case 1:
                empilhar(pilha);
                break;
            case 2:
                listar(pilha);
                break;
            case 3:
                desempilhar(pilha);
                break;
            case 4:
                esvaziar(pilha);
                break;
            default:
                printf("Digite um numero valido!");
                break;
        }
    } while (resp != 0);
    
    return 0;
}

void empilhar(Pilha *pilha) {
    if (pilha->topo == 9) {
        printf("Pilha cheia, tente outra opcao");
    } else {
        int n;
        printf("Informe o valor inteiro para empilhar: ");
        scanf("%d", &n);
        pilha->vetor[pilha->topo] = n;
        pilha->topo++;
    }
}

void listar(Pilha *pilha) {
    system("clear");
    
    if (pilha->topo == 0) {
    	printf("Pilha vazia");
	} else {
		printf("Quantia de elementos: %d\n", pilha->topo);
		for (int i = pilha->topo-1;i >= 0;i--) {
        	printf("\t[%d]\n", pilha->vetor[i]);
    	}
    }
    
    sleep(5);
}

void desempilhar(Pilha *pilha) {
    if (pilha->topo > 0) {
    	pilha->vetor[pilha->topo] = NULL;
    	pilha->topo--;
	} else {
		printf("Pilha ja esta vazia");
	}
}

void esvaziar(Pilha *pilha){
    for (int i = pilha->topo;i >= 0;i--) {
        pilha->vetor[i] = NULL;
    }
    
    pilha->topo = 0;
}

/*-----------------------------------------------------------*/

/*
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

// LISTA
// Implementar os metodos: inserir, exibir, busca_sequencial, inserir_ordem

typedef struct {
    int vetor[10];
    int index;
}Lista;

void inserir(Lista *lista);
void exibir(Lista *lista);
void busca_sequencial(Lista *lista);
void inserir_ordem(Lista *lista);

int main() {
    Lista lista[0];
    lista->index = 0;

    int resp;
    do {
        system("clear");
        printf("\nInforme o que fazer:\n[1] - inserir   [2] - exibir (5 segundos)   [3] - buscar posicao   [4] - inserir em posicao   [0] - sair\n");
        scanf("%d", &resp);


        switch (resp) {
        	case 0:
        		exit(0);
        		break;
            case 1:
                inserir(lista);
                break;
            case 2:
                exibir(lista);
                break;
            case 3:
                busca_sequencial(lista);
                break;
            case 4:
                inserir_ordem(lista);
                break;
            default:
                printf("Digite um numero valido!");
                break;
        }
    } while (resp != 0);

    return 0;
}

void inserir(Lista *lista) {
    if (lista->index == 9) {
        printf("Fila cheia");
        sleep(2);
    } else {
        int n;
        printf("Informe um numero inteiro para inserir: ");
        scanf("%d", &n);
        lista->vetor[lista->index] = n;
        lista->index++;
    }
}

void exibir(Lista *lista) {
    if (lista->index == 0) {
        printf("Fila vazia");
    } else {
        for (int i = 0;i <= lista->index-1;i++) {
            printf("[%d]\t", lista->vetor[i]);
        }
    }
}

void busca_sequencial(Lista *lista) {
    int n;
    system("clear");
    printf("Informe um numero inteiro para buscar: ");
    scanf("%d", &n);
    for (int i = 0;i < lista->index;i++) {
        if (lista->vetor[i] == n) {
            printf("O valor buscado esta na posicao %d", i+1);
            break;
        }
    }
    sleep(3);
}

void inserir_ordem(Lista *lista) {
	system("clear");
	int n, pos, v2[10], ind2 = 0, aux = 0, add = 0;
    printf("Informe um numero inteiro para inserir: ");
    scanf("%d", &n);
    printf("Informe a posicao onde inserir: (a partir em 1)");
    scanf("%d", &pos);
    
    if (lista->index == 9) {
        printf("Lista cheia");
        sleep(2);
        return;
    }
    
    lista->index++;
    
    while (aux <= lista->index) {
    	if (aux == pos-1) {
    	    v2[aux] = n;
    	    aux++;
    	    v2[aux] = lista->vetor[aux-1];
    	} else if (aux < pos-1) {
    	    v2[aux] = lista->vetor[aux];
    	    
    	} else {
    	    v2[aux] = lista->vetor[aux-1];
    	}
    	aux++;
	}
	
	for (int i = 0;i < lista->index;i++) {
    	lista->vetor[i] = v2[i];
    	printf("[%d]", lista->vetor[i]);
	}
}
*/

/*-------------------------------------------------------------------*/

/*
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

// Fila
// Implementar os metodos: enfileirar, exibir, desenfileirar

typedef struct {
    int vetor[15];
    int index;
} Fila;

void enfileirar(Fila *fila);
void exibir(Fila *fila);
void desenfileirar(Fila *fila);

int main() {
    Fila fila[0];
    fila ->index = 0;

    int resp;
    do {
        system("clear");
        printf("\nInforme o que fazer:\n[1] - enfileirar   [2] - exibir (5 segundos)   [3] - desenfileirar  [0] - sair\n");
        scanf("%d", &resp);

        switch (resp) {
            case 0:
                exit(0);
            case 1:
                enfileirar(fila);
                break;
            case 2:
                exibir(fila);
                break;
            case 3:
                desenfileirar(fila);
                break;
            default:
                printf("Digite um numero valido!");
                break;
        }
    } while (resp != 0);


    return 0;
}

void enfileirar(Fila *fila) {
    int n;
    printf("Informe um inteiro para enfileirar: ");
    scanf("%d", &n);
    fila->vetor[fila->index] = n;
    fila->index++;
}

void exibir(Fila *fila) {
    if (fila->index == 0) {
        printf("Fila vazia");
    } else {
        for (int i = 0;i < fila->index;i++) {
            printf("[%d]  ", fila->vetor[i]);
        }
    }
}

void desenfileirar(Fila *fila) {
    if (fila->index == 0) {
        printf("Fila vazia");
    } else {
        fila->index--;
        int i = 0;
        while (i < fila->index) {
            fila->vetor[i] = fila->vetor[i + 1];
            i++;
        }
    }
}
*/