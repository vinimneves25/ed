#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

// PILHA
// Implementar os metodos: empilhar, exibir, desempilhar_topo, esvaziar

typedef struct {
    int topo;
    int vetor[10];
}Pilha;

void empilhar(Pilha *pilha);
void listar(Pilha *pilha);
void desempilhar(Pilha *pilha);
void esvaziar(Pilha *pilha);

int main() {
    Pilha pilha[0];
    pilha->topo = 0;
    
    int resp;
    do {
        system("clear");
        printf("\nInforme o que fazer:\n[1] - empilhar   [2] - exibir (5 segundos)   [3] - desempilhar   [4] - esvaziar   [0] - sair\n");
        scanf("%d", &resp);
        
        switch (resp) {
            case 0:
                continue;
            case 1:
                empilhar(pilha);
                break;
            case 2:
                listar(pilha);
                break;
            case 3:
                desempilhar(pilha);
                break;
            case 4:
                esvaziar(pilha);
                break;
            default:
                printf("Digite um numero valido!");
                break;
        }
    } while (resp != 0);
    
    return 0;
}

void empilhar(Pilha *pilha) {
    if (pilha->topo == 9) {
        printf("Pilha cheia, tente outra opcao");
    } else {
        int n;
        printf("Informe o valor inteiro para empilhar: ");
        scanf("%d", &n);
        pilha->vetor[pilha->topo] = n;
        pilha->topo++;
    }
}

void listar(Pilha *pilha) {
    system("clear");
    
    if (pilha->topo == 0) {
    	printf("Pilha vazia");
	} else {
		printf("Quantia de elementos: %d\n", pilha->topo);
		for (int i = pilha->topo-1;i >= 0;i--) {
        	printf("\t[%d]\n", pilha->vetor[i]);
    	}
    }
    
    sleep(5);
}

void desempilhar(Pilha *pilha) {
    if (pilha->topo > 0) {
    	pilha->vetor[pilha->topo] = NULL;
    	pilha->topo--;
	} else {
		printf("Pilha ja esta vazia");
	}
}

void esvaziar(Pilha *pilha){
    for (int i = pilha->topo;i >= 0;i--) {
        pilha->vetor[i] = NULL;
    }
    
    pilha->topo = 0;
}

