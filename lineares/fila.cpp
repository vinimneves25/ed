#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

// Fila
// Implementar os metodos: enfileirar, exibir, desenfileirar

typedef struct {
    int vetor[15];
    int index;
} Fila;

void enfileirar(Fila *fila);
void exibir(Fila *fila);
void desenfileirar(Fila *fila);

int main() {
    Fila fila[0];
    fila ->index = 0;

    int resp;
    do {
        system("clear");
        printf("\nInforme o que fazer:\n[1] - enfileirar   [2] - exibir (5 segundos)   [3] - desenfileirar  [0] - sair\n");
        scanf("%d", &resp);

        switch (resp) {
            case 0:
                exit(0);
            case 1:
                enfileirar(fila);
                break;
            case 2:
                exibir(fila);
                break;
            case 3:
                desenfileirar(fila);
                break;
            default:
                printf("Digite um numero valido!");
                break;
        }
    } while (resp != 0);


    return 0;
}

void enfileirar(Fila *fila) {
    int n;
    printf("Informe um inteiro para enfileirar: ");
    scanf("%d", &n);
    fila->vetor[fila->index] = n;
    fila->index++;
}

void exibir(Fila *fila) {
    if (fila->index == 0) {
        printf("Fila vazia");
    } else {
        for (int i = 0;i < fila->index;i++) {
            printf("[%d]  ", fila->vetor[i]);
        }
    }
}

void desenfileirar(Fila *fila) {
    if (fila->index == 0) {
        printf("Fila vazia");
    } else {
        fila->index--;
        int i = 0;
        while (i < fila->index) {
            fila->vetor[i] = fila->vetor[i + 1];
            i++;
        }
    }
}