#include <stdio.h>
#include <conio.h>

// Divisao por subtracao sucessiva

int subtracao(int n1, int n2, int cont);

int main()
{
    int n1, n2, total, cont = 0;
    
    scanf("%d", &n1);
    scanf("%d", &n2);
    
    total = subtracao(n1, n2, cont);


    printf("O total eh: %d", total);
    return 0;
}

int subtracao(int n1, int n2, int cont) {
    if ((n2 - n1) >= 0) {
        cont++;
        return subtracao(n1, n2 - n1, cont);
    } else {
        return cont;
    }
}