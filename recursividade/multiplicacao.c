#include <stdio.h>
#include <conio.h>

// Multplicacao com somas sucessivas (recursiva)

int soma(int n1, int n2);

int main()
{
    int n1, n2, total;
    
    scanf("%d", &n1);
    scanf("%d", &n2);
    
    total = soma(n1, n2);


    printf("O total eh: %d", total);
    return 0;
}

int soma(int n1, int n2) {
    if (n1 > 0) {
        return n2 + soma(n1 - 1, n2);
    } else {
        return 0;
    }
}