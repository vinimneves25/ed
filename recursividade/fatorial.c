#include <stdio.h>
#include <conio.h>

// Fatorial por multiplicao sucessiva

double fatorial(int n);

int main(void) {
    int n1;
    double fat;
  
    scanf("%d", &n1);
  
    fat = fatorial(n1);
  
    printf("O total eh: %.0lf", n1, fat);
  
    return 0;
}

double fatorial(int n) {
    if (n <= 1) {
        return 1;
    } else {
        return n * fatorial(n - 1);
    }
}